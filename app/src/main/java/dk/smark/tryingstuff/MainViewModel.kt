package dk.smark.tryingstuff

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class MainViewModel: ViewModel() {

    val name: MutableLiveData<String> = MutableLiveData()

    init {
        name.value = "Someones name"
    }
}